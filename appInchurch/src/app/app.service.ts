import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppService {
  private url = 'https://reqres.in/api/';
  private shared_user_id = new BehaviorSubject<Object>(null);
  current_user_id = this.shared_user_id.asObservable();

  emitUserChange = new EventEmitter<number>();
  emitNewUser = new EventEmitter<Boolean>();

<<<<<<< HEAD
  httpConfigHeaders = {
    header: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  }

=======
>>>>>>> user-form
  constructor(private http: HttpClient) { }

  getApi(parameter) {
    return this.http.get(this.url + parameter);
  }

  postApi(parameter, obj){
<<<<<<< HEAD
    return this.http.post(this.url + parameter, obj, this.httpConfigHeaders);
  }

  putApi(parameter, obj){
    return this.http.put(this.url + parameter, obj, this.httpConfigHeaders);
  }

=======
    return this.http.post(this.url + parameter, obj);
  }

  putApi(parameter, obj){
    return this.http.put(this.url + parameter, obj);
  }

>>>>>>> user-form
  deleteApi(parameter) {
    return this.http.delete(this.url + parameter );
  }

  selectUserDatail(userid: number) {
    this.emitUserChange.emit(userid);
  }

  newUserForm(){
    this.emitNewUser.emit();
  }

}
