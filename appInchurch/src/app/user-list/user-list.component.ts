import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  private listRequest = 'users?page=';
  public pages_total;
  public current_page;
  public current_field: any;
  public current_pagination: any;

  constructor( private service: AppService) { }

  ngOnInit() {
    this.current_field = {
      id: null,
      first_name: null,
      last_name: null,
      avatar: null
    };
    this.service.getApi(this.listRequest).subscribe(
      data =>  {
        this.pages_total = new Array(data.total_pages).fill(1);
        this.current_page = data.data;
      },
      error => console.log(error)
    );
    this.service.emitNewUser.subscribe(
      res => this.current_field = 0
    )
  }

  selectPage(val: number) {
    this.current_pagination = val;
    val = val + 1;
    console.log(val);
    this.service.getApi(this.listRequest + val).subscribe(
      data =>  { this.current_page = data.data; },
      error => alert(error)
    )
  }

  selectUser(id: number) {
    this.service.selectUserDatail(id);
  }

  selectField(val: any) {
    console.log(val);
    this.current_field = val;
  }

}
