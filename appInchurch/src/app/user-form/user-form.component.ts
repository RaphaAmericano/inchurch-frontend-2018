import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl,  Validators, FormGroup } from '@angular/forms';

import { AppService } from '../app.service';

import { User } from '../model/user.model';
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  userForm: FormGroup;

  new_user: User = new User();
  submitStatus:any;

  constructor(private service: AppService, private formb: FormBuilder ) {}

  ngOnInit() {

    this.userForm = this.formb.group({
      firstName: [null, [Validators.required, Validators.min(3)]],
      lastName: [null, [Validators.required, Validators.min(2)]],
      job: [null, [Validators.required, Validators.min(3)]]
    });

    this.service.emitUserChange.subscribe(
      res => this.service.getApi('users/' + res).subscribe(
        val => {
          this.userForm.addControl('id', new FormControl(val.id, Validators.required));
          this.userForm.patchValue({ firstName: val.data.first_name });
          this.userForm.patchValue({ lastName: val.data.last_name });
          this.userForm.patchValue({ job: val.data.job });
        }
      )
    );

  }

  onSubmit() {

    this.new_user.name = this.userForm.value.firstName +' '+ this.userForm.value.lastName;
    this.new_user.job = this.userForm.value.job;

    if(this.userForm.get('id')) {
      this.service.putApi('users/' + this.userForm.value.id, this.new_user).subscribe(
        res => console.log(res),
        err => console.log(err)
      );

    } else {
      this.service.postApi('users', this.new_user).subscribe(
        res => {
          if(!this.userForm.get('id')) {
            this.userForm.addControl('id', new FormControl(res.id, Validators.required));
            }
            this.submitStatus = 'create'
          },
        error => console.log(error)
      );
    }
  }

  deleteUser(val: number) {
    this.service.deleteApi('users/'+val).subscribe(
      result => {
        this.submitStatus = 'delete';
        console.log(result)
      },
      error => console.log(error)
    );
  }

  updateUser(val: number) {
    let obj = {
      name: this.userForm.get('firstName').value + this.userForm.get('lastName').value,
      job: this.userForm.get('job').value
    };
    this.service.putApi('user/' + val, obj).subscribe(
      data =>  {
        this.submitStatus = 'update',
        console.log(data)
      },
      error => console.log(error)
    );
  }

  removeIdControl(){
    this.userForm.removeControl('id');
    this.submitStatus = null;
    this.service.newUserForm();
  }

  fieldValid( field: string ){
    return !this.userForm.get(field).valid && this.userForm.get(field).touched;
  }

  formFieldClass(field: string){
    return {
      'is-success': this.fieldValid(field),
      'is-danger': this.fieldValid(field)
    }
  }



}
